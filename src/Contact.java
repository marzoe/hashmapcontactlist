/**
 * @author Mariele, Zoe
 */
public class Contact {
    String name;
    String number, homePhone, workPhone;
    String address;
    String email;
    
    public Contact(String name, String number){
        this.name = name;
        this.number = number;
    }
    
    //needs a constructor that takes all params
    public Contact(String name, String number, String address, String email, String homePhone, String workPhone){
        this.name = name;
        this.number = number;
        this.address = address;
        this.email = email;
        this.homePhone = homePhone;
        this.workPhone = workPhone;
    }
    
    //needs tostring method, pretty print
    @Override
    public String toString(){
        return ("Name: " + this.name + "; Number: " + this.number + "; Address: " + this.address + "; Email: " + this.email + "; Home: " + this.homePhone + "; Work: " + this.workPhone);
    }
    
    String getName(){
        return this.name;
    }
    
    String getNumber(){
        return this.number;
    }
    
    void addAddress(String addr){
        this.address = addr;
    }
    
    void addEmail(String email){
        this.email = email;
    }
    
    void addHomePhone(String hPhone){
        this.homePhone = hPhone;
    }
    
    void addWorkPhone(String wPhone){
        this.workPhone = wPhone;
    }
}
