
import java.math.BigInteger;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

//
// CS 124 contact list based on Java hashmap
// Mariele, Zoe
//
// Necessary functions:
// Insert key, value pair by key or value
// Remove key, value pair by key or value
// Get key, value pair by key or value
//
//Implementation: Java HashMap
//
/**
 *
 *
 */
public class Hashmapcontactlist<G> {
    HashMap nameMap;
    HashMap numberMap;
    int size;
    
    //constructor
    public Hashmapcontactlist(int initialCapacity){
        nameMap = new HashMap(initialCapacity);
        numberMap = new HashMap(initialCapacity);
        size = this.size();
    }
    
    //default constructor, maybe not needed
    public Hashmapcontactlist(){
        nameMap = new HashMap();
        numberMap = new HashMap();
        size = this.size();
    }
    
    //Method to remove nonnumerical characters from phone number and validate phone number
    public String processNumber(String number){
        number = number.replaceAll("[^\\d]", "");
        return number;
    }
    
    //removes all k,v pairs from the map
    /*
     * RT Analysis: O(1)
     */
    public void emptyMap(){
        nameMap.clear();
        numberMap.clear();
    }
    
  /*Insert method: no duplicates, no null, 1 name per number    
     * Analysis: RT = O(1)
     * put() is O(1) and insert() calls put for each hash map but no loop so still O(1) = overall RT O(1)
     */
    public boolean insert(String name, String number){  //why is this boolean??
        //add the pair to insert to both maps with the appropriate item as key
        //nameMap has names as keys
        //numberMap has numbers as keys
        String pNum = processNumber(number);
        Contact toPut = new Contact(name, pNum);
        nameMap.put(name, toPut);
        numberMap.put(pNum, toPut);
        size++;
        return true;
    }
    
    //Zoe
    //Get method: return name or return number
  //Get method: return name or return number
    /*
     * RT Analysis: get() is O(1). find() has no loop so running time is
     * O(1) = O(1)
     */
    public String find(G nameORnumber) {
        Contact result;
        
        //if the parameter is a key in the name map, get the value from that map
        if(nameMap.containsKey(nameORnumber)){
            result = (Contact)nameMap.get(nameORnumber);
            return result.number;
        
        //if the parameter is a key in the number map, get the value from that map
        } else if (numberMap.containsKey(nameORnumber)) {
            result = (Contact)numberMap.get(nameORnumber);
            return result.name;
        
        } else {
            return "Key not found";
        }
    }
    
    //Mariele
  //Delete method: remove k,v pair from map by getting it by name or number
    /*
     * RT Analysis: delete() calls containsKey() and runs in O(1) time with an efficient hashing function.
       Overall RT for delete() = O(1)
     */
    public boolean delete(G nameORnumber) {
        Contact aContact;
        //delete k,v pair from both name and number maps
    	/*if nameMap contains key of the nameORnumber entered, it means that the key is the
    	 name and the value is the number, so we have to delete the name key from nameMap and
    	 the number key from numberMap
    	 */
    	/***
    	 * get(Object key) -Returns the value to which the specified key is mapped,
 		 * or null if this map contains no mapping for the key.
    	 */
    	if(nameMap.containsKey(nameORnumber))
    	{
    		aContact = (Contact)nameMap.remove(nameORnumber);
    		// now use get(Object key) which returns the value to which the specified key is 
    		// mapped (or null if this map contains no mapping for the key)
    		// and delete the number associated with the name from the numberMap
    		//numberMap.remove(numberMap.get(nameORnumber));
                numberMap.remove(aContact.number);
                size--;
                return true;
    	}
    	// else if numberMap contains the key nameORnumber, it means that nameORnumber is
    	// a number, so remove the key from numberMap, then use get(Object key) to determine
    	//the key of the name associated with said number in the nameMap, and remove it there 
    	else if (numberMap.containsKey(nameORnumber))
    	{       
            /*removing to resolve error
    		numberMap.remove(nameORnumber);
    		nameMap.remove(nameMap.get(nameORnumber));
            */
            
            aContact = (Contact)numberMap.remove(nameORnumber);
            nameMap.remove(aContact.name);
            size--;
            return true;
    	}
    	//else 
    		//throw error exception? account for error? is this necessary?
    	//return true;
        return false;
    }
    
    //Iterate through the table and print every available k,v pair
    //prints from name, could print from number but doesn't. Uncomment lines 133, 137 and 141
    //to add printing from number 
    //I don't think that printing from number is helpful and so those lines are commented out
    /*
     RT Analysis: O(n) because iterator has to go through entire hash map in order to 
     * access each data item to print it. 
     */
    public void printAllContacts(){
        Set pairSet = nameMap.entrySet();
        Set pairSetNum = numberMap.entrySet();
        
        //iterate through the set of map entries
        Iterator pairIter = pairSet.iterator();
        //Iterator pairIterNum = pairSetNum.iterator();
        
        while(pairIter.hasNext()){
            System.out.println(pairIter.next());
            //System.out.println(pairIterNum.next());
        }
    }
    
    //size of hashmap - get number of k,v pairs, increments with insert
    /* RT Analysis: O(1)
     * 
     */
    public int size(){
        if(nameMap.size() == numberMap.size()){
            return nameMap.size();
        }
        return -1;
        
    }
    
    
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        //additional testing
        
        /*the insert method handles names and numbers, but the print all method handles full contacts and the
        contact toString method prints the full contact. Changing the contact toString method or the insert method to support either a full information implementation
        or a limited name and number implementation would make our solution better-presented.
        */
        //Create an instance of the object - test the constructor
        Hashmapcontactlist thisMap = new Hashmapcontactlist();
        
        //Populate the object - test insert, including insertion of phone numbers with invalid characters
        thisMap.insert("Lisa Simpson", "000111-----%%%%4444$$");
        thisMap.insert("Bart Simpson", "1112223333");
        thisMap.insert("Maggie Simpson", "5555555555");
        thisMap.insert("Homer Simpson", "1231231234");
        thisMap.insert("Grandpa Simpson", "8888*****55533&&&&4");
        thisMap.insert("Liz Lemon", "111222ffhhjj4433!");
        thisMap.insert("Jon Snow", "8882223333");
        thisMap.insert("Arya Stark", "4445556666");
        
        //print all of the contacts to show that they were added
        thisMap.printAllContacts();
        
        //Use the size/print number of contacts method to verify that the number of contacts stored in the map is correct
        //There should be 8 contacts stored.
        System.out.println("Number of contacts: " + thisMap.size() + "\n");
        
        //delete a few contacts by both name and number
        thisMap.delete("Jon Snow");
        thisMap.delete("5555555555");
        thisMap.delete("8888555334");
        
        //print all of the contacts to show that there were deletions
        thisMap.printAllContacts();
        
        //Use the size/print number of contacts method to verify that the number of contacts stored in the map is correct
        //after the deletions. There should be 5 contacts stored.
        System.out.println("Number of contacts: " + thisMap.size() + "\n");
        
        //Find one of the contacts by name and number
        System.out.println(thisMap.find("Lisa Simpson"));
        System.out.println(thisMap.find("0001114444"));
        System.out.println(thisMap.find("5555555555"));
        System.out.println(thisMap.find("0000000000"));
        
        //Verify with size and find that it is cleared
        //Error code -1 because of the issue where insert and delete do not properly change the size
        System.out.println("\nNumber of contacts: " + thisMap.size());
        
        //Empty the map
        thisMap.emptyMap();
        
        //Verify with size and find that it is cleared
        System.out.println("\nNumber of contacts: " + thisMap.size());
        
    }
    
}
