import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @Mariele, Zoe
 */
public class HashmapcontactlistTest {
    
    public HashmapcontactlistTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }
    
    //test class instance variables
    String kN = "3235752290";
    String k1N = "5102338294";
    String k2N = "5102438794";
    String k3N = "5102538194";
    String k4N = "5102638994";
    String k5N = "5102738095";
    
    //Tests method to remove non-numeric characters from String inputs that should be numeric
    @Test
    public void testProcess(){
        Hashmapcontactlist hTest = new Hashmapcontactlist();
        String num1 = "(123)456-7890";
        String num2 = "123.456.7890";
        String num3 = "(123) 456 7890";
        
        assertTrue(hTest.processNumber(num1).equals("1234567890"));
        assertTrue(hTest.processNumber(num2).equals("1234567890"));
        assertTrue(hTest.processNumber(num3).equals("1234567890"));
    }
    
    
    /**
     * Mariele's test case
     */
    @Test
    public void testInsert() {
        // Should this be variable type Contact or Hashmapcontactlist??
        Hashmapcontactlist hTest = new Hashmapcontactlist();
        
        System.out.println("Test adding to map");
        
        //Hashmapcontactlist instance = new Hashmapcontactlist();
        hTest.insert("Katniss Everdeen", kN);
        hTest.insert("Angelina Johnson", k1N);
        hTest.insert("Nicki Bell", k2N);
        hTest.insert("Dominique Castillo", k3N);
        hTest.insert("Catalina Ramirez", k4N);
        
        // how do we check the contains() method here? nameMap.containsKey
        // Check for Contact Catalina Ramirez, k4N
        assertTrue(hTest.nameMap.containsKey("Catalina Ramirez"));
        assertTrue(hTest.numberMap.containsKey(k4N));
        
        //Check for Contact Katniss Everdeen, kN
        assertTrue(hTest.nameMap.containsKey("Katniss Everdeen"));
        assertTrue(hTest.numberMap.containsKey(kN));
        
        //Check for Angelina Johnson, k1N
        assertTrue(hTest.nameMap.containsKey("Angelina Johnson"));
        assertTrue(hTest.numberMap.containsKey(k1N));
        
        //Check for Nicki Bell, k2N
        assertTrue(hTest.nameMap.containsKey("Nicki Bell"));
        assertTrue(hTest.numberMap.containsKey(k2N));
        
        //Check for Dominique Castillo, k3N
        assertTrue(hTest.nameMap.containsKey("Dominique Castillo"));
        assertTrue(hTest.numberMap.containsKey(k3N));
    }
    
    //use to test methods other than insert to avoid duplication
    //helper method to create and populate a Hashmapcontactlist object
    public Hashmapcontactlist testObject(){
        Hashmapcontactlist hTest = new Hashmapcontactlist();
        hTest.insert("Katniss Everdeen", kN);
        hTest.insert("Angelina Johnson", k1N);
        hTest.insert("Nicki Bell", k2N);
        hTest.insert("Dominique Castillo", k3N);
        hTest.insert("Catalina Ramirez", k4N);
        
        return hTest;
    }
    
    /**
     * Test of size method, of class Hashmapcontactlist.
     */
    @Test
    public void testSize() {
        Hashmapcontactlist hTest = testObject();
        
        System.out.println("size");
        
        //test object adds 5 items
        int expResult = 5;
        
        int result = hTest.size();
        assertEquals(expResult, result);
    }
    

    /**
     * Test of find method, of class Hashmapcontactlist.
     */
    @Test
    // Test find. Map is empty so should come back false.
    public void testFind() {
        Hashmapcontactlist hTest = testObject();
        
        hTest.insert("Katniss Everdeen", kN);
        hTest.insert("Angelina Johnson", k1N);
        hTest.insert("Nicki Bell", k2N);
        hTest.insert("Dominique Castillo", k3N);
        hTest.insert("Catalina Ramirez", k4N);
        
        String expResult = k3N;
        String result = hTest.find("Dominique Castillo");
        assertEquals(expResult, result);
        
        String expResult2 = "Dominique Castillo";
        String result2 = hTest.find(k3N);
        assertEquals(expResult2, result2);
    }
    
    
    /**
     * Test of delete method, of class Hashmapcontactlist.
     */
    @Test
    public void testDelete() {
        Hashmapcontactlist hTest = testObject();
        
        System.out.println("delete");
        
        boolean expResult = true;
        boolean result = hTest.delete("Dominique Castillo");
        assertEquals(expResult, result);
        
        //assertion about size;
        assertEquals(hTest.size(),4);
    }

    /**
     * Test of printAllContacts method, of class Hashmapcontactlist.
     */
    @Test
    public void testPrintAllContacts() {
    //reference: https://stackoverflow.com/questions/32241057/how-to-test-a-print-method-in-java-using-junit
    //Dakshinamurthy Karra
        Hashmapcontactlist hTest = testObject();
        
        System.out.println("printAllContacts");
        hTest.printAllContacts();
        
        //Expected outcome
        //Katniss Everdeen=Name: Katniss Everdeen; Number: 3235752290; Address: null; Email: null; Home: null; Work: null
        //Angelina Johnson=Name: Angelina Johnson; Number: 5102338294; Address: null; Email: null; Home: null; Work: null
        //Dominique Castillo=Name: Dominique Castillo; Number: 5102538194; Address: null; Email: null; Home: null; Work: null
        //Catalina Ramirez=Name: Catalina Ramirez; Number: 5102638994; Address: null; Email: null; Home: null; Work: null
        //Nicki Bell=Name: Nicki Bell; Number: 5102438794; Address: null; Email: null; Home: null; Work: null

        //needs modification so it actually tests the print method
        //create a list of items that should be in the printed output for this test
        ArrayList<Contact> cList = new ArrayList<Contact>();
        
        //populate the list
        Contact c1 = new Contact("Katniss Everdeen", kN);
        Contact c2 = new Contact("Angelina Johnson", k1N);
        Contact c3 = new Contact("Nicki Bell", k2N);
        Contact c4 = new Contact("Dominique Castillo", k3N);
        Contact c5 = new Contact("Catalina Ramirez", k4N);
        
        cList.add(c1);
        cList.add(c2);
        cList.add(c3);
        cList.add(c4);
        cList.add(c5);
        
        //Save System.out
        PrintStream oldSysOut = System.out;
        
        //Use a ByteArrayOutputStream to get the print call output
        ByteArrayOutputStream thisBaos = new ByteArrayOutputStream();
        
        //Point System.out to the baos
        System.setOut(new PrintStream(thisBaos));     //commenting out so test class compiles
        
        //print the arraylist with the test data
        System.out.print(cList);
        
        //reset System.out
        System.setOut(oldSysOut);
        
        //thisBaos should have the print output
        String SysOutPrint = new String(thisBaos.toByteArray());
        
        //Assertions
        assertTrue(SysOutPrint.contains("Name: Katniss Everdeen"));
        assertTrue(SysOutPrint.contains("Name: Angelina Johnson"));
        assertTrue(SysOutPrint.contains("Name: Nicki Bell"));
        assertTrue(SysOutPrint.contains("Name: Dominique Castillo"));
        assertTrue(SysOutPrint.contains("Name: Catalina Ramirez"));
        assertTrue(SysOutPrint.contains("Number: " + kN));
        assertTrue(SysOutPrint.contains("Number: " + k1N));
        assertTrue(SysOutPrint.contains("Number: " + k2N));
        assertTrue(SysOutPrint.contains("Number: " + k3N));
        assertTrue(SysOutPrint.contains("Number: " + k4N));
    }

    

    /**
     * Test of emptyMap method, of class Hashmapcontactlist.
     */
    @Test
    public void testEmptyMap() {
        Hashmapcontactlist hTest = testObject();
        
        System.out.println("emptyMap");
        hTest.emptyMap();
        // instance should be an empty Hashmapcontactlist, so check that it is empty
        assertTrue(hTest.numberMap.isEmpty());
        assertTrue(hTest.nameMap.isEmpty());
    }
}
